package com.rocbin.autovalidator.beans;

import java.util.Arrays;

import com.rocbin.autovalidator.options.ParamOption;
import com.rocbin.autovalidator.types.ParamType;

/**
 * 参数校验选项
 * @author Rocbin
 *
 */
public class Param {

	private String key; // 参数名称
	private ParamType type; // 校验类型
	private ParamOption option; // 校验选项
	private String[] additional; // 附加校验参数

	public Param(String key, ParamType type, ParamOption option,
			String[] additional) {
		super();
		this.key = key;
		this.type = type;
		this.option = option;
		this.additional = additional;
	}

	public String getKey() {
		return key;
	}

	public ParamType getType() {
		return type;
	}

	public ParamOption getOption() {
		return option;
	}

	public String[] getAdditional() {
		return additional;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setType(ParamType type) {
		this.type = type;
	}

	public void setOption(ParamOption option) {
		this.option = option;
	}

	public void setAdditional(String[] additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "Param [key=" + key + ", type=" + type + ", option=" + option
				+ ", additional=" + Arrays.toString(additional) + "]";
	}

	
}
