package com.rocbin.autovalidator.types;

public enum ParamType {
	STRING, // string
	INTEGER, // int
	LONG, // long
	BOOLEAN, // double
	DATE, // date
	EMAIL, // email
	URL, // url
	REGEX // regex
}
