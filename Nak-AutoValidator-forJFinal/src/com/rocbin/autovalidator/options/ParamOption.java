package com.rocbin.autovalidator.options;

public enum ParamOption {
	REQUIRED, // required
	OPTION, // option
	EQUALS // equals
}
