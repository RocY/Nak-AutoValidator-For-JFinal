package com.rocbin.autovalidator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.PathKit;
import com.jfinal.render.Render;
import com.rocbin.autovalidator.validates.AbstractValidator;
import com.rocbin.autovalidator.validates.ValidatorFactory;
import com.rocbin.autovalidator.xml.AutoValidatorConfig;
import com.rocbin.autovalidator.xml.AutoValidatorConfigXMLLoader;
import com.rocbin.autovalidator.xml.ParamConfigSet;

/**
 * 自动化参数校验拦截器<br/>
 * 它使用XML配置多组Action的字段校验设置，可以校验参数值大小范围（长度范围），取值范围，正则校验等<br/>
 * XML文件位置：<Strong>/WEB-INF/auto-validator-config.xml</Strong><br/>
 * <strong style="color:#f00;">注意：不要把XML暴露，因为XML一旦被黑了，后果不知道会怎样哦。</strong>
 * @author Rocbin [asboot@qq.com]
 *
 */
public class AutoParamValidator implements Interceptor {

	private static AutoValidatorConfig config = null;
	private final HashMap<String, List<AbstractValidator>> VALIDATOR_CACHE = new HashMap<>();
	private HashMap<String, String> errs = new HashMap<>();
	private boolean init = false;

	protected synchronized void loadConfig() {
		String xmlConfigFile = PathKit.getWebRootPath()
				+ "/WEB-INF/auto-validator-config.xml";
		config = AutoValidatorConfigXMLLoader.loadFromXMLFile(xmlConfigFile);
	}

	@Override
	public void intercept(ActionInvocation ai) {
		if (init == false) {
			loadConfig();
			init = true;
		}
		if (config == null) {
			System.err
					.println("AutoValidator严重异常:无法加载XML配置，AutoValidator 无法正常工作！");
			ai.invoke();
			return;
		}
		AutoValidatorXMLRes annotation = ai.getMethod().getAnnotation(
				AutoValidatorXMLRes.class);
		StringBuilder sb = new StringBuilder();
		String resId = null;
		if (annotation == null || annotation.resId() == null
				|| "".equals(annotation.resId())) {
			System.err
					.println(String
							.format("AutoValidator：Controller<%1$s>.Action<%2$s>上面没有配置AutoValidator注解或者注解的参数(redId=\"%3$s\")无效！",
									ai.getControllerKey(), ai.getMethodName(),
									annotation.resId()));
			// 缺省注解，将使用ControllerKey + "/" + ActionMethodName作为resId
			sb.append(ai.getControllerKey()).append("/")
					.append(ai.getActionKey());
			resId = sb.toString();
		} else {
			resId = annotation.resId();
		}
		List<AbstractValidator> validators = VALIDATOR_CACHE.get(resId);
		ParamConfigSet ps = config.getParamConfigSet(resId);
		if (validators == null) {
			if (ps != null) {
				// TODO new validator to cache
				validators = newValidatorsToCache(ps);
				VALIDATOR_CACHE.put(resId, validators);
				validateActionParams(ai, validators);
			} else {
				// TODO not config the auto-validator, skip validation!
				ai.invoke();
				return;
			}
		} else {
			// 校验
			validateActionParams(ai, validators);
		}
		if (!errs.isEmpty()) {
			handleParamsValidateFailed(ai);
			errs.clear(); // 清除已处理完毕的错误。
		} else {
			ai.invoke(); // 校验参数全部成功
		}
	}

	/**
	 * @param ai
	 * @param validators
	 */
	private void validateActionParams(ActionInvocation ai,
			List<AbstractValidator> validators) {
		for (AbstractValidator paramValidator : validators) {
			boolean validate = paramValidator.valid(ai.getController());
			if (!validate) {
				addError(paramValidator.getParamName(),
						paramValidator.getErrMessage());
			}
		}
	}

	protected void addError(String errKey, String message) {
		// TODO 参数校验失败时，添加错误提示信息处理
		errs.put(errKey, message);
	}

	/**
	 * 处理参数校验结果<br/>
	 * 重写这个方法实现自己的控制逻辑。<br/>
	 * 你可以在这里添加代码，控制校验结果输出等等
	 */
	protected void handleParamsValidateFailed(ActionInvocation ai) {
		final String output = JsonKit.toJson(errs);
		ai.getController().render(new Render() {
			private static final long serialVersionUID = 3847988685290578301L;

			@Override
			public void render() {
				this.response.setStatus(400);
				this.response.setCharacterEncoding("utf-8");
				this.response.addHeader("Content-Type",
						"text/html; charset=utf-8");
				try {
					this.response.getWriter().append(output);
				} catch (IOException ignore) {
				}
			}

		});
	}

	protected List<AbstractValidator> newValidatorsToCache(ParamConfigSet ps) {
		ValidatorFactory factory = ValidatorFactory.newInstance(ps);
		return factory.getValidators();
	}

}
