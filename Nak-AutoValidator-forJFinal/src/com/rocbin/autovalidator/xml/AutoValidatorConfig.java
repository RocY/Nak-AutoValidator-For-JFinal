package com.rocbin.autovalidator.xml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AutoValidatorConfig {
	private HashMap<String, ParamConfigSet> config = new HashMap<String, ParamConfigSet>();

	public void addParamConfigSet(String resId, ParamConfigSet paramConfigSet) {
		config.put(resId, paramConfigSet);
	}

	public ParamConfigSet getParamConfigSet(String resId) {
		return config.get(resId);
	}

	public boolean isContainParamConfigSet(String resId) {
		return config.containsKey(resId);
	}
	
	public Map<String, ParamConfigSet> getConfigMap() {
		return Collections.unmodifiableMap(config);
	}
	
	public boolean isEmpty() {
		return config.isEmpty();
	}
}
