package com.rocbin.autovalidator.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.rocbin.autovalidator.options.ParamOption;
import com.rocbin.autovalidator.types.ParamType;

public class ParamConfig {

	/*
	 * 请求参数名称
	 */
	private String name;
	/*
	 * 比较的另一个参数名称
	 */
	private String equalTo;
	/*
	 * 请求参数类型
	 */
	private ParamType type = ParamType.STRING;
	/**
	 * 请求参数选项{必选，可选，与另一个字段比较}
	 */
	private ParamOption option = ParamOption.REQUIRED;
	/*
	 * 最大值
	 */
	private String max;
	/*
	 * 最小值
	 */
	private String min;
	/*
	 * 参数值范围
	 */
	private List<String> range = new ArrayList<String>();
	/*
	 * 参数错误提示消息
	 */
	private String errMessage;
	/*
	 * 附加选项：验证类型
	 */
	/*
	 * 验证最大值最小值
	 */
	private boolean isMaxminValidation = false;
	/*
	 * 验证值的指定范围
	 */
	private boolean isRangeValidation = false;

	/*
	 * 日期格式(type为date时可用)
	 */
	private String dateFormat = "yyyy-MM-dd";
	/*
	 * 正则表达式字符串(type为regex时可用)
	 */
	private String regx = "";

	public ParamConfig() {
	}

	public ParamConfig(String name, ParamType type, ParamOption option,
			String max, String min, List<String> range,
			boolean isMaxminValidation, boolean isRangeValidation) {
		super();
		this.name = name;
		this.type = type;
		this.option = option;
		this.max = max;
		this.min = min;
		this.range = range;
		this.isMaxminValidation = isMaxminValidation;
		this.isRangeValidation = isRangeValidation;
	}

	public String getEqualName() {
		return equalTo;
	}

	public void setEqualName(String equalName) {
		this.equalTo = equalName;
	}

	public String getName() {
		return name;
	}

	public ParamType getType() {
		return type;
	}

	public ParamOption getOption() {
		return option;
	}

	public String getMax() {
		return max;
	}

	public String getMin() {
		return min;
	}

	public List<String> getRange() {
		return Collections.unmodifiableList(range);
	}

	public boolean isMaxminValidation() {
		return isMaxminValidation;
	}

	public boolean isRangeValidation() {
		return isRangeValidation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(ParamType type) {
		this.type = type;
	}

	public void setOption(ParamOption option) {
		this.option = option;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public void setMin(String min) {
		this.min = min;
	}

	// public void setRange(List<String> range) {
	// this.range = range;
	// }

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public void addRangeValue(String value) {
		this.range.add(value);
	}

	public void setMaxminValidation(boolean isMaxminValidation) {
		this.isMaxminValidation = isMaxminValidation;
	}

	public void setRangeValidation(boolean isRangeValidation) {
		this.isRangeValidation = isRangeValidation;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ParamConfig [name=");
		builder.append(name);
		builder.append(", equalTo=");
		builder.append(equalTo);
		builder.append(", type=");
		builder.append(type);
		builder.append(", option=");
		builder.append(option);
		builder.append(", max=");
		builder.append(max);
		builder.append(", min=");
		builder.append(min);
		builder.append(", range=");
		builder.append(range);
		builder.append(", errMessage=");
		builder.append(errMessage);
		builder.append(", isMaxminValidation=");
		builder.append(isMaxminValidation);
		builder.append(", isRangeValidation=");
		builder.append(isRangeValidation);
		builder.append("]");
		return builder.toString();
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getRegx() {
		return regx;
	}

	public void setRegx(String regx) {
		this.regx = regx;
	}

}
