package com.rocbin.autovalidator.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.rocbin.autovalidator.xml.handler.AutoValidatorConfigXMLHandler;

public final class AutoValidatorConfigXMLLoader {

	public static final AutoValidatorConfig loadFromXMLFile(String path) {
		AutoValidatorConfig config = null;
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			AutoValidatorConfigXMLHandler handler = new AutoValidatorConfigXMLHandler();
			parser.parse(new File(path), handler);
			config = handler.getResult();
		}catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return config;
	}
}
