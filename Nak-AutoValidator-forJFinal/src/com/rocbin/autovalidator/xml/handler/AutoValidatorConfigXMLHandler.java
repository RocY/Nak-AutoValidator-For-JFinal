package com.rocbin.autovalidator.xml.handler;

import java.lang.ref.SoftReference;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.rocbin.autovalidator.options.ParamOption;
import com.rocbin.autovalidator.types.ParamType;
import com.rocbin.autovalidator.xml.AutoValidatorConfig;
import com.rocbin.autovalidator.xml.ParamConfig;
import com.rocbin.autovalidator.xml.ParamConfigSet;

public class AutoValidatorConfigXMLHandler extends DefaultHandler {

	private static final String NODE_AUTO_VALIDATOR_CONFIG = "auto-validator-config";
	private static final String NODE_CONFIG = "config";
	private static final String NODE_PARAM = "param";
	private static final String NODE_NAME = "name";
	private static final String NODE_EQUAL_TO = "equals";
	private static final String NODE_TYPE = "type";
	private static final String NODE_OPTIONAL = "optional";
	private static final String NODE_MAX = "max";
	private static final String NODE_MIN = "min";
	private static final String NODE_RANGE = "range";
	private static final String NODE_VALUE = "value";
	private static final String NODE_MSG = "msg";
	private static final String NODE_DATE_FORMAT = "dateFormat";
	private static final String NODE_REGEX = "regex";
	private static final String ATTR_RESID = "resid";

	private static final String DEFAULT_MSG = "%1$s参数不正确。";

	private SoftReference<AutoValidatorConfig> mConfig = new SoftReference<>(
			new AutoValidatorConfig());

	private StringBuilder sb = new StringBuilder();
	private boolean startedNodeRange = false;

	private SoftReference<ParamConfigSet> pcs = null;
	private SoftReference<ParamConfig> param = null;

	public AutoValidatorConfig getResult() {
		return mConfig.get();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		switch (qName) {
		case NODE_AUTO_VALIDATOR_CONFIG: {
			// start.
		}
			break;
		case NODE_CONFIG: {
			// config start.
			pcs = new SoftReference<>(new ParamConfigSet());
			pcs.get().setResId(attributes.getValue(ATTR_RESID));
		}
			break;
		case NODE_PARAM: {
			// param start.
			param = new SoftReference<>(new ParamConfig());
		}
			break;
		case NODE_NAME: {
			// name start.
		}
		case NODE_EQUAL_TO: {

		}
			break;
		case NODE_TYPE: {
			// type start.
		}
			break;
		case NODE_OPTIONAL: {
			// optional start.
		}
			break;
		case NODE_MAX: {
			// max start.
			param.get().setMaxminValidation(true);
		}
			break;
		case NODE_MIN: {
			// min start.
			param.get().setMaxminValidation(true);
		}
			break;
		case NODE_RANGE: {
			// range start.
			param.get().setRangeValidation(true);
		}
		case NODE_MSG: {
			// err message
		}
		case NODE_DATE_FORMAT: {
			// date-format for type[date]
		}
		case NODE_REGEX: {
			// regex for (regular expression) type[regex]
		}
			break;
		case NODE_VALUE: {
			// value start.
			startedNodeRange = true;
		}
			break;
		default: {
			System.err.println("auto validator config 未知的xml配置属性：" + qName);
		}
		}
		sb.delete(0, sb.length()); // clear the text buffer
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		String textValue = sb.toString();
		ParamConfig paramConfig = param.get();
		switch (qName) {
		case NODE_NAME: {
			// name end.
			paramConfig.setName(textValue);
		}
			break;
		case NODE_EQUAL_TO: {
			// name end.
			paramConfig.setEqualName(textValue);
		}
			break;
		case NODE_TYPE: {
			// type end.
			try {
				paramConfig.setType(ParamType.valueOf(textValue.toUpperCase()));
			} catch (IllegalArgumentException e) {
				throw new SAXException("无效的type配置值：" + textValue);
			}
		}
			break;
		case NODE_OPTIONAL: {
			// optional end.
			try {
				paramConfig.setOption(ParamOption.valueOf(textValue
						.toUpperCase()));
			} catch (IllegalArgumentException e) {
				throw new SAXException("无效的optional配置值：" + textValue);
			}
		}
			break;
		case NODE_MAX: {
			// max end.
			paramConfig.setMax(textValue);
		}
			break;
		case NODE_MIN: {
			// min end.
			paramConfig.setMin(textValue);
		}
			break;
		case NODE_RANGE: {
			// range end.
			startedNodeRange = false;
		}
			break;
		case NODE_MSG: {
			// err message
			paramConfig.setErrMessage(textValue);
		}
			break;
		case NODE_DATE_FORMAT: {
			// date-format
			paramConfig.setDateFormat(textValue);
		}
			break;
		case NODE_REGEX: {
			// regex
			paramConfig.setRegx(textValue);
		}
			break;
		case NODE_VALUE: {
			// value end.
			if (startedNodeRange) {
				paramConfig.addRangeValue(textValue);
			} else {
				throw new SAXException(
						"auto validator config xml配置不正确:value必须按这样的配置<range><value>123</value></range>");
			}
		}
			break;
		case NODE_PARAM: {
			// param end.
			if (paramConfig.getErrMessage() == null) {
				paramConfig.setErrMessage(String.format(DEFAULT_MSG,
						paramConfig.getName()));
			}
			pcs.get().addParamConfig(paramConfig);
		}
			break;
		case NODE_CONFIG: {
			// config end.
			mConfig.get().addParamConfigSet(pcs.get().getResId(), pcs.get());
		}
			break;
		case NODE_AUTO_VALIDATOR_CONFIG: {
			// auto-validator-config end.
			sb = null;
		}
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		sb.append(ch, start, length);
	}

}
