package com.rocbin.autovalidator.xml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParamConfigSet {

	private String resId;
	private HashMap<String, ParamConfig> paramConfigSet = new HashMap<String, ParamConfig>();

	public void addParamConfig(ParamConfig paramConfig) {
		paramConfigSet.put(paramConfig.getName(), paramConfig);
	}

	public ParamConfig getParamConfig(String paramName) {
		return paramConfigSet.get(paramName);
	}

	public boolean isContainParamConfig(String paramName) {
		return paramConfigSet.containsKey(paramName);
	}

	public Map<String, ParamConfig> getConfigMap() {
		return Collections.unmodifiableMap(paramConfigSet);
	}

	public boolean isEmpty() {
		return paramConfigSet.isEmpty();
	}

	public String getResId() {
		return resId;
	}

	public void setResId(String resId) {
		this.resId = resId;
	}
}
