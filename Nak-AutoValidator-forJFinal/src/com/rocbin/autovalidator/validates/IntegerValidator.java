package com.rocbin.autovalidator.validates;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class IntegerValidator extends AbstractValidator {

	public IntegerValidator(ParamConfig config) {
		super(config);
	}

	/**
	 * 验证一个必选的整型参数<br/>
	 * 可选校验取值范围{最大值-最小值}，或者校验取值的指定范围{A,B,C}<br/>
	 */
	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		Integer value = null;
		try {
			value = c.getParaToInt(getConfig().getName());
		} catch (Exception e) {
			validate = false;
		}
		validate = required(value);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @return
	 */
	private boolean required(Integer value) {
		boolean validate = true;
		if (value == null) {
			validate = false;
			return validate;
		}
		if (getConfig().isMaxminValidation()) {
			int max = Integer.MAX_VALUE;
			int min = Integer.MIN_VALUE;
			if (getConfig().getMax() != null) {
				max = Integer.valueOf(getConfig().getMax());
			}
			if (getConfig().getMin() != null) {
				min = Integer.valueOf(getConfig().getMin());
			}
			validate = value >= min && value <= max;
		}
		// 不支持max-min和range方式同时配置
		if (getConfig().isRangeValidation()) {
			validate = false;
			for (String eq : getConfig().getRange()) {
				if (value == Integer.valueOf(eq)) {
					validate = true;
					break;
				}
			}
		}
		return validate;
	}

	/**
	 * 不支持整型参数的校验选项{option}
	 */
	@Override
	protected boolean optionValidate(Controller c) {
		throw new UnsupportedOperationException(
				"AutoValidator 配置异常：IntegerValidator 验证integer类型不支持 option 选项:"
						+ getConfig());
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		Integer valueOne = null;
		Integer valueTwo = null;
		boolean validateOne = false;
		boolean validateTwo = false;
		try {
			valueOne = c.getParaToInt(getConfig().getName());
		} catch (Exception e) {
			validateOne = false;
			validate = false;
			return validate;
		}
		try {
			valueTwo = c.getParaToInt(getConfig().getEqualName());
		} catch (Exception e) {
			validateTwo = false;
			validate = false;
			return validate;
		}
		validateOne = required(valueOne);
		validateTwo = required(valueTwo);
		validate = validateOne && validateTwo
				&& valueOne.intValue() == valueTwo.intValue();
		return validate;
	}

}
