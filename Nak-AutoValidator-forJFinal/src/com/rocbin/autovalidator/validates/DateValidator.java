package com.rocbin.autovalidator.validates;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class DateValidator extends AbstractValidator {

	private SimpleDateFormat sf = null;

	public DateValidator(ParamConfig config) {
		super(config);
	}

	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		String format = getConfig().getDateFormat();
		Date date = parseDate(format, value);
		validate = required(format, date);
		validate = lengthValidate(value, format);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @param format
	 * @return
	 */
	private boolean lengthValidate(String value, String format) {
		/*
		 * simple date format 解析日期当遇到如下情况时： new
		 * SimpleDateFormat("yyyy-MM-dd").parse("2014-01-01xxxx")
		 * 日期输入后面明显不符合格式，但依然被认为是正确的<br/> 下面加上长度验证。限制日期的最大长度，不能超过配置的长度
		 */
		boolean validate = value.length() == format.length();
		return validate;
	}

	/**
	 * @param validate
	 * @param format
	 * @param date
	 * @return
	 */
	private boolean required(String format, Date date) {
		boolean validate = true;
		if (date == null) {
			validate = false;
		} else {
			Calendar cd = Calendar.getInstance();
			cd.setTime(date);
			if (getConfig().isMaxminValidation()) {
				Date maxDate = parseDate(format, getConfig().getMax());
				Date minDate = parseDate(format, getConfig().getMin());
				if (cd.before(minDate) || cd.after(maxDate)) {
					validate = false;
				}
			} else if (getConfig().isRangeValidation()) {
				for (String eq : getConfig().getRange()) {
					Date eqDate = parseDate(format, eq);
					Calendar eqCd = Calendar.getInstance();
					eqCd.setTime(eqDate);
					if (cd.compareTo(eqCd) == 0) {
						validate = true;
						break;
					}
				}
			}
		}
		return validate;
	}

	private Date parseDate(String format, String value) {
		if (sf == null) {
			sf = new SimpleDateFormat(format);
		}
		try {
			return sf.parse(value);
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	protected boolean optionValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		if (value == null) {
			validate = false;
		} else if ("".equals(value)) {
			validate = true;
		} else {
			String format = getConfig().getDateFormat();
			Date date = parseDate(format, value);
			validate = required(format, date);
			validate = lengthValidate(value, format); // 附加长度校验，严格模式
		}
		return validate;
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		boolean validateOne = false;
		boolean validateTwo = false;
		String format = getConfig().getDateFormat();
		String valueOne = c.getPara(getConfig().getName());
		if (valueOne == null) {
			return false;
		}
		Date dateOne = parseDate(format, valueOne);
		String valueTwo = c.getPara(getConfig().getEqualName());
		if (valueTwo == null) {
			return false;
		}
		Date dateTwo = parseDate(format, valueTwo);
		if (dateOne == null || dateTwo == null) {
			validate = false;
		} else {
			validateOne = required(format, dateOne);
			validateOne = lengthValidate(valueOne, format); // 附加长度校验，严格模式
			validateTwo = required(format, dateTwo);
			validateTwo = lengthValidate(valueOne, format); // 附加长度校验，严格模式
			Calendar cd = Calendar.getInstance();
			cd.setTime(dateOne);
			long dateOneMS = cd.getTimeInMillis();
			cd.setTime(dateTwo);
			long dateTwoMS = cd.getTimeInMillis();
			// validate A and validate B and A = B
			validate = validateOne && validateTwo && dateOneMS == dateTwoMS;
		}
		return validate;
	}

}
