package com.rocbin.autovalidator.validates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class RegexValidator extends AbstractValidator {

	public RegexValidator(ParamConfig config) {
		super(config);
	}

	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		validate = required(value);
		return validate;
	}

	/**
	 * @param value
	 * @return
	 */
	private boolean required(String value) {
		boolean validate;
		if (value == null) {
			validate = false;
		} else {
			Pattern p = Pattern.compile(getConfig().getRegx());
			Matcher m = p.matcher(value);
			validate = m.matches();
		}
		return validate;
	}

	@Override
	protected boolean optionValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		if (value == null) {
			validate = false;
		} else if ("".equals(value)) {
			validate = true;
		} else {
			validate = required(value);
		}
		return validate;
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		String valueOne = c.getPara(getConfig().getName());
		String valueTwo = c.getPara(getConfig().getEqualName());
		boolean validateOne = false;
		boolean validateTwo = false;
		if(valueOne == null || valueTwo == null) {
			validate = false;
		} else {
			validateOne = required(valueOne);
			validateTwo = required(valueTwo);
			validate = validateOne && validateTwo && valueOne.equals(valueTwo);
		}
		return validate;
	}

}
