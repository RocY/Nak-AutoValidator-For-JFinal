package com.rocbin.autovalidator.validates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class EmailValidator extends AbstractValidator {
	private static final String emailAddressPattern = "\\b(^['_A-Za-z0-9-]+(\\.['_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";

	public EmailValidator(ParamConfig config) {
		super(config);
	}

	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		validate = required(value);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @return
	 */
	private boolean required(String value) {
		boolean validate = false;
		if (value == null) {
			validate = false;
		} else {
			Pattern pattern = Pattern.compile(emailAddressPattern,
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(value);
			if (matcher.matches()) {
				validate = true;
				if (getConfig().isMaxminValidation()) {
					int max = Integer.valueOf(getConfig().getMax());
					int min = Integer.valueOf(getConfig().getMin());
					if (value.length() < min || value.length() > max) {
						validate = false;
					} else {
						validate = true;
					}
				} else if (getConfig().isRangeValidation()) {
					validate = false;
					// 支持限制使用某些域名的邮箱
					// 限制为QQ邮箱："@qq.com"网易邮箱："@163.com"
					for (String eq : getConfig().getRange()) {
						if (value.endsWith(eq)) {
							validate = true;
							break;
						}
					}
				}
			}
		}
		return validate;
	}

	/**
	 * 可选的Email参数校验<br/>
	 * 参数可以为空字符串，但绝对不可以是null，否则一定要符合配置的Email规则
	 */
	@Override
	protected boolean optionValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		if (value == null) {
			validate = false;
		} else if ("".equals(value)) {
			validate = true;
		} else {
			validate = required(value);
		}
		return validate;
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		String EmailOne = c.getPara(getConfig().getName());
		String EmailTwo = c.getPara(getConfig().getEqualName());
		if (EmailOne == null || EmailTwo == null) {
			validate = false;
		} else {
			boolean validateOne = false;
			boolean validateTwo = false;
			validateOne = required(EmailOne);
			validateTwo = required(EmailTwo);
			validate = validateOne && validateTwo && EmailOne.equalsIgnoreCase(EmailTwo);
		}
		return validate;
	}

}
