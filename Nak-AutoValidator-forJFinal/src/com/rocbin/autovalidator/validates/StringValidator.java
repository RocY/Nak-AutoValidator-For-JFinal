package com.rocbin.autovalidator.validates;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class StringValidator extends AbstractValidator {

	public StringValidator(ParamConfig config) {
		super(config);
	}

	/**
	 * 校验一个必选字符串参数<br/>
	 * 长度校验：Min-Max(length>=min && length<=max)<br/>
	 * ,(min默认值1，max默认值Integer.MAX_VALUE) 取值范围校验：range<A,B,C> 区分大小写，比较是否在 A,B,C
	 * 范围内
	 * 
	 */
	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		validate = required(value);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @return
	 */
	private boolean required(String value) {
		boolean validate = false;
		if (getConfig().isMaxminValidation()) {
			int minLength = 1;
			int maxLength = Integer.MAX_VALUE;
			if (getConfig().getMin() != null) {
				minLength = Integer.valueOf(getConfig().getMin());
			}
			if (getConfig().getMax() != null) {
				maxLength = Integer.valueOf(getConfig().getMax());
			}
			validate = value != null
					&& (value.trim().length() >= minLength && value.trim()
							.length() <= maxLength);
		}
		if (getConfig().isRangeValidation()) {
			validate = false;
			for (String eq : getConfig().getRange()) {
				if (eq.equals(value)) {
					validate = true;
					break;
				}
			}
		}
		return validate;
	}

	/**
	 * 校验一个可选字符串参数<br/>
	 * 参数不能为 null,但可以是空字符串<br/>
	 * 如果配置长度校验，则return value.length<=max <br/>
	 * 如果配置取值范围，则校验值是否在配置范围内
	 */
	@Override
	protected boolean optionValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		if (value == null) {
			validate = false;
			return validate;
		}
		if ("".equals(value)) {
			validate = true;
		}
		if (getConfig().isMaxminValidation()) {
			int maxLength = Integer.MAX_VALUE;
			if (getConfig().getMax() != null) {
				maxLength = Integer.valueOf(getConfig().getMax());
			}
			validate = value.length() >= maxLength;
		}
		if (getConfig().isRangeValidation()) {
			for (String eq : getConfig().getRange()) {
				if (eq.equals(value)) {
					validate = true;
					break;
				}
			}
		}
		return validate;
	}

	/**
	 * 将两个字符串参数进行规则比较，并且比较它们是否相等<br/>
	 * return requiredString(one) && requiredString(two) && one.equals(two)<br/>
	 * 按照必选参数规则校验两个字符串：<br/>
	 * 长度校验：value.trim()>=min &&
	 * value.trim()<=max,(min默认值1，max默认值Integer.MAX_VALUE)<br/>
	 * 取值范围校验：range<A,B,C>，取值必须在range范围内。
	 * 
	 */
	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		String valueOne = c.getPara(getConfig().getName());
		String valueTwo = c.getPara(getConfig().getEqualName());
		boolean validateOne = required(valueOne);
		boolean validateTwo = required(valueTwo);
		validate = validateOne && validateTwo && valueOne.equals(valueTwo);
		return validate;
	}

}
