package com.rocbin.autovalidator.validates;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

/**
 * 抽象参数校验类<br/>
 * 实现了校验选项{必选，可选，比较}的选择校验方法{requiredValidate,optionValidate,equalsValidate}<br/>
 * 建议所有的Validator都直接继承实现该类的方法
 * 
 * @author Rocbin
 * 
 */
public abstract class AbstractValidator {

	private ParamConfig config;
	private String paramName;
	private String equalTo;
	private String errMessage;

	public AbstractValidator(ParamConfig config) {
		this.config = config;
		paramName = config.getName();
		equalTo = config.getEqualName();
		errMessage = config.getErrMessage();
	}

	public String getErrMessage() {
		return errMessage;
	}

	public boolean valid(Controller c) {
		boolean validate = false;
		switch (config.getOption()) {
		case REQUIRED: {
			validate = requiredValidate(c);
		}
			break;
		case OPTION: {
			validate = optionValidate(c);
		}
			break;
		case EQUALS: {
			validate = equalsValidate(c);
		}
		}
		return validate;
	}

	protected ParamConfig getConfig() {
		return config;
	}

	public String getParamName() {
		return paramName;
	}

	public String getEqualTo() {
		return equalTo;
	}

	/**
	 * 必选参数-校验
	 * 
	 * @return 校验结果
	 */
	protected abstract boolean requiredValidate(Controller c);

	/**
	 * 可选参数-校验
	 * 
	 * @return 校验结果
	 */
	protected abstract boolean optionValidate(Controller c);

	/**
	 * 与另一字段比较
	 * 
	 * @return 校验结果
	 */
	protected abstract boolean equalsValidate(Controller c);
}
