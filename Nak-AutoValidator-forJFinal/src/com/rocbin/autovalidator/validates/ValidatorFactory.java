package com.rocbin.autovalidator.validates;

import java.util.ArrayList;
import java.util.List;

import com.rocbin.autovalidator.xml.ParamConfig;
import com.rocbin.autovalidator.xml.ParamConfigSet;

public final class ValidatorFactory {

	// private ValidatorFactory INSTANCE;
	private ParamConfigSet config;

	private ValidatorFactory(ParamConfigSet ps) {
		this.config = ps;
	}

	public static ValidatorFactory newInstance(ParamConfigSet ps) {
		return new ValidatorFactory(ps);
	}

	public List<AbstractValidator> getValidators() {
		List<AbstractValidator> list = new ArrayList<>();
		for (ParamConfig param : config.getConfigMap().values()) {
			list.add(newParamValidator(param));
		}
		return list;
	}

	private AbstractValidator newParamValidator(ParamConfig config) {
		AbstractValidator validator = null;
		switch (config.getType()) {
		case STRING: {
			// validate type string
			validator = new StringValidator(config);
		}
			break;
		case INTEGER: {
			// validate type int
			validator = new IntegerValidator(config);
		}
			break;
		case LONG: {
			// validate type long
			validator = new LongValidator(config);
		}
			break;
		case BOOLEAN: {
			// validate type double
			validator = new BooleanValidator(config);
		}
			break;
		case DATE: {
			// validate type date
			validator = new DateValidator(config);
		}
			break;
		case EMAIL: {
			// validate type email
			validator = new EmailValidator(config);
		}
			break;
		case URL: {
			// validate type url
			validator = new UrlValidator(config);
		}
			break;
		case REGEX: {
			// validate type regex
			validator = new RegexValidator(config);
		}
			break;
		}

		return validator;
	}
}
