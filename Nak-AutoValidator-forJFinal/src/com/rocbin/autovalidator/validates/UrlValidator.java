package com.rocbin.autovalidator.validates;

import java.net.MalformedURLException;
import java.net.URL;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class UrlValidator extends AbstractValidator {

	public UrlValidator(ParamConfig config) {
		super(config);
	}
	
	private URL parseURL(String value) {
		URL url = null;
		try {
			url = new URL(value);
		} catch (MalformedURLException e) {
			url = null;
		}
		return url;
	}

	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		validate = required(value);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @return
	 */
	private boolean required(String value) {
		boolean validate =true;
		if(value == null) {
			validate = false;
		} else {
			if(value.toLowerCase().startsWith("https://")) {
				value = "http://".concat(value.substring(8));
			}
			URL url = parseURL(value);
			if(url == null) {
				validate =false;
			} else {
				if(getConfig().isMaxminValidation()) {
					int max = Integer.valueOf(getConfig().getMax());
					int min = Integer.valueOf(getConfig().getMin());
					if(value.length()< min || value.length() > max) {
						validate =false;
					} else {
						validate = true;
					}
				}
				if(getConfig().isRangeValidation()) {
					validate = false;
					for(String eq : getConfig().getRange()) {
						URL eqURL = parseURL(eq);
						if(eqURL.getHost().toLowerCase().equals(url.getHost().toLowerCase())) {
							validate =true;
							break;
						}
					}
				}
			}
		}
		return validate;
	}

	@Override
	protected boolean optionValidate(Controller c) {
		boolean validate = false;
		String value = c.getPara(getConfig().getName());
		if(value == null) {
			validate = false;
		} else if("".equals(value)) {
			validate = true;
		} else {
			validate = required(value);
		}
		return validate;
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		String valueOne = c.getPara(getConfig().getName());
		String valueTwo = c.getPara(getConfig().getEqualName());
		boolean validateOne = false;
		boolean validateTwo = false;
		if(valueOne == null || valueTwo == null) {
			validate = false;
		} else {
			validateOne = required(valueOne);
			validateTwo = required(valueTwo);
			validate = validateOne && validateTwo && valueOne.toLowerCase().endsWith(valueOne.toLowerCase());
		}
		return validate;
	}

}
