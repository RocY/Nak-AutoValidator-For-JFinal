package com.rocbin.autovalidator.validates;

import com.jfinal.core.Controller;
import com.rocbin.autovalidator.xml.ParamConfig;

public class BooleanValidator extends AbstractValidator {

	public BooleanValidator(ParamConfig config) {
		super(config);
	}

	/**
	 * 验证一个必选的布尔型参数<br/>
	 * boolean值混略大小写只能是{true,false}，不能为null或其他字符串
	 * 不支持max-min，但支持取值范围校验{true,false}，rang正确只能配置一个取值
	 */
	@Override
	protected boolean requiredValidate(Controller c) {
		boolean validate = false;
		Boolean value = null;
		String booleanStr = c.getPara(getConfig().getName());
		if(booleanStr == null) {
			validate = false;
			return validate;
		}
		if ("true".equals(booleanStr.toLowerCase())
				|| "false".equals(booleanStr.toLowerCase())) {
			validate = true;
			value = c.getParaToBoolean(getConfig().getName());
		} else {
			validate = false;
			value = false;
			return validate;
		}
		validate = required(value);
		return validate;
	}

	/**
	 * @param validate
	 * @param value
	 * @return
	 */
	private boolean required(Boolean value) {
		boolean validate = true;
		if (value == null) {
			validate = false;
			return validate;
		}

		if (getConfig().isRangeValidation()) {
			validate = false;
			for (String eq : getConfig().getRange()) {
				if (value == Boolean.valueOf(eq)) {
					validate = true;
					break;
				}
			}
		}
		return validate;
	}

	/**
	 * 不支持整型参数的校验选项{option}
	 */
	@Override
	protected boolean optionValidate(Controller c) {
		throw new UnsupportedOperationException(
				"AutoValidator 配置异常：BooleanValidator 验证Boolean类型不支持 option 选项:"
						+ getConfig());
	}

	@Override
	protected boolean equalsValidate(Controller c) {
		boolean validate = false;
		Boolean valueOne = null;
		Boolean valueTwo = null;
		boolean validateOne = false;
		boolean validateTwo = false;
		String booleanStr = c.getPara(getConfig().getName());
		if(booleanStr == null) {
			validateOne = false;
			validate = false;
			return validate;
		}
		if ("true".equals(booleanStr.toLowerCase())
				|| "false".equals(booleanStr.toLowerCase())) {
			validate = true;
			valueOne = c.getParaToBoolean(getConfig().getName());
		} else {
			validate = false;
			valueOne = false;
			return validate;
		}
		String booleanStr2 = c.getPara(getConfig().getEqualName());
		if(booleanStr2 == null) {
			validateTwo = false;
			validate = false;
			return validate;
		}
		if ("true".equals(booleanStr2.toLowerCase())
				|| "false".equals(booleanStr2.toLowerCase())) {
			validate = true;
			valueTwo = c.getParaToBoolean(getConfig().getEqualName());
		} else {
			validate = false;
			valueTwo = false;
			return validate;
		}
		validateOne = required(valueOne);
		validateTwo = required(valueTwo);
		validate = (validateOne && validateTwo) && (valueOne == valueTwo);
		return validate;
	}

}
