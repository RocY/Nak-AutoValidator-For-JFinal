package com.rocbin.test.controllers;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.rocbin.autovalidator.AutoParamValidator;
import com.rocbin.autovalidator.AutoValidatorXMLRes;

/**
 * 测试用首页控制器
 * @author Rocbin
 * 
 */
public class IndexController extends Controller {

	public void index() {
		test();
	}

	@AutoValidatorXMLRes(resId="/test")
	@Before(AutoParamValidator.class)
	public void test() {
		renderText("<h1>Hello! test.</h1>");
	}
	
	@AutoValidatorXMLRes(resId="/test2")
	@Before(AutoParamValidator.class)
	public void test2() {
		renderText("<h1>Hello! test.</h1>");
	}
	
	@AutoValidatorXMLRes(resId="/test3")
	@Before(AutoParamValidator.class)
	public void test3() {
		renderText("<h1>Hello! test.</h1>");
	}
}
