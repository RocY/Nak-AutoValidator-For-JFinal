package com.rocbin.test.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.render.ViewType;
import com.rocbin.test.controllers.IndexController;

/**
 * Web configs
 * 
 * @author Rocbin asboot@qq.com
 * 
 */
public class WebConfig extends JFinalConfig {

	public static void main(String[] args) {
		JFinal.start();
	}

	@Override
	public void configConstant(Constants me) {
		// 基础配置
		me.setDevMode(true);
		me.setViewType(ViewType.JSP);
		me.setEncoding("utf-8");
	}

	@Override
	public void configHandler(Handlers me) {
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configPlugin(Plugins me) {
	}

	@Override
	public void configRoute(Routes me) {
		// 配置控制器路由
		me.add("/", IndexController.class);
	}
}
